// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"io"
)

// AIAssistant proxy client for Google Assistant
type AIAssistant struct {
	respWriter io.Writer
}

// Tell is text to be sent to the google assistant
func (as *AIAssistant) Tell(text string, expectUserResponse bool, contexts []Context) {

	f := &Fullfilment{Speech: text, DisplayText: text, Source: "NY Bus Time", ContextOut: contexts}
	f.Data.Google.ExpectUserResponse = expectUserResponse

	encoder := json.NewEncoder(as.respWriter)
	encoder.Encode(f)
}

func (as *AIAssistant) SendRichResponse(textToSpeech, title, subTitle, formattedText, url, urlTitle string) {

	rich := buildRichResponse(textToSpeech, title, subTitle, formattedText, url, urlTitle)
	encoder := json.NewEncoder(as.respWriter)
	encoder.Encode(rich)

}


