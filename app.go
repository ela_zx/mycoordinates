// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"cloud.google.com/go/datastore"
)

// action names
const (
	actionHandlePermissionGranted = "handle_permission_granted"
)

// http
const (
	httpContentType     = "Content-type"
	httpContentTypeJSON = "application/json; charset=utf-8"
	httpAuthorization   = "Authorization"
	httpAllow           = "Allow"
)

func main() {
	http.HandleFunc("/", handler)
}

func handler(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.Header().Set(httpAllow, http.MethodPost)
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	
	// temp log
	// Save a copy of this request for debugging.
	requestDump, err := httputil.DumpRequest(r, true)

	//ctx := appengine.NewContext(r)
	ctx := context.Background()
	client, err := datastore.NewClient(ctx,"my-coordinates-v")

	if err != nil {
		log.Fatal(err.Error())
  		//log.Debugf(ctx, err.Error())
	}
	log.Println(string(requestDump))
	//log.Debugf(ctx, string(requestDump))
	// temp log

	aiasst := &AIAssistant{w}
	airesp := &AIResponse{}

	w.Header().Set(httpContentType, httpContentTypeJSON)

	decoder := json.NewDecoder(r.Body) // always use Decoder instead of marshaler when dealing with io.Reader!
	if er := decoder.Decode(airesp); er != nil {
		//log.Printf("app error: json-decode: %s", er.Error())
		fmt.Printf("app error: json-decode: %s", er.Error())
		w.WriteHeader(http.StatusBadRequest)
		//aiasst.Tell("JSON Error", false, nil)
		aiasst.Tell(er.Error(), false, nil)
		return
	}

	switch airesp.Result.Action {
	case actionHandlePermissionGranted:

		if !airesp.PermissionGranted(){
			aiasst.Tell("I can not operate with out your permission", false, nil)
			return
		}

		lat := fmt.Sprintf("%v", airesp.OriginalRequest.Data.Device.Location.Coordinates.Latitude)
		lon := fmt.Sprintf("%v", airesp.OriginalRequest.Data.Device.Location.Coordinates.Longitude)
		
		formattedText := fmt.Sprintf("%s,%s", lat, lon)
		url := fmt.Sprintf("http://maps.google.com/maps?q=%s,%s", lat, lon)
		
		ds := &DataStore { ctx, client }
		user := NewUserEntity(airesp.OriginalRequest.Data.User.ID,
			airesp.OriginalRequest.Data.Device.Location.Coordinates.Latitude,
			airesp.OriginalRequest.Data.Device.Location.Coordinates.Longitude, url)
		ds.saveUserEntity(user)	

		aiasst.SendRichResponse("Here is your location that I got from the assistant", "GPS Coordinates", "Latitude,Longitude", formattedText, url, "Open with maps")

		return
	default:
		aiasst.Tell("Unknown action", false, nil)
	}
}
