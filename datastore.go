// Copyright 2018 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// GCP API requires go modules!
// go mod init YOUR_MODULE_NAME bitbucket.org/ela_zx/mycoordinates

import (
	"context"
	"time"

	"cloud.google.com/go/datastore"
)

const kind string = "UserLog"

// DataStore type to represent google datastore in gcp.
type DataStore struct {
	context context.Context
	client datastore.Client
}

// UserEntity (..).Save()
type UserEntity struct {
	userID    string    //`datastore:"userId"`
	Location  datastore.GeoPoint  `datastore:"location"`
	MapURL    string    `datastore:"mapUrl,noindex"`
	CreatedOn time.Time `datastore:"createdOn"`
	UpdatedOn time.Time `datastore:"updatedOn"`
	Version   int64     `datastore:"version"`
}

// NewUserEntity creates new user entity
func NewUserEntity(uid string, lat float64, lng float64, url string) *UserEntity {
	u := UserEntity{userID: uid, MapURL: url}

	u.Location.Lat = lat
	u.Location.Lng = lng
	u.UpdatedOn = time.Now()
	u.CreatedOn = time.Now()
	u.Version = 1

	return &u
}

// Save it to data store
func (ds *DataStore) saveUserEntity(ent *UserEntity) error {

	old := &UserEntity{userID: ent.userID}
	if ds.loadUserEntity(old) {
		ent.Version = old.Version + 1
		ent.CreatedOn = old.CreatedOn
	}

	key := datastore.NameKey(kind, ent.userID, nil)   
	//NewKey(ds.context, kind, ent.userID, 0, nil)
	_, err := ds.client.Put(ds.context, key, ent)
	return err
}

func (ds *DataStore) loadUserEntity(ent *UserEntity) (ok bool) {

	key := datastore.NameKey(kind, ent.userID,  nil)
	if err := ds.client.Get(ds.context, key, ent); err != nil {
		return false
	}
	return true
}
