// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

type originalRequest struct {
	Data struct {
		Surface struct {
			Capabilities []struct {
				Name string `json:"name"`
			} `json:"capabilities"`
		} `json:"surface"`
		Inputs []struct {
			Intent    string `json:"intent"`
			Arguments []struct {
				Name      string `json:"name"`
				TextValue string `json:"textValue"`
				//BoolValue bool `json:"boolValue,omitempty"`
			} `json:"arguments"`
		} `json:"inputs"`
		User struct {
			ID string `json:"userId"`
		} `json:"user"`
		Device struct {
			Location struct {
				Coordinates struct {
					Latitude  float64 `json:"latitude"`
					Longitude float64 `json:"longitude"`
				} `json:"coordinates"`
			} `json:"location"`
		} `json:"device"`
	} `json:"data"`
}
