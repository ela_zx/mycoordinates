// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// Context represents ApiAi Context.
type Context struct {
	Name       string                 `json:"name"`
	Lifespan   int                    `json:"lifespan"`
	Parameters map[string]interface{} `json:"parameters,omitempty"`
}

// NewContext creates new Context{name,5,map{}}
func NewContext(name string) Context {
	return Context{name, 5, map[string]interface{}{}}
}
