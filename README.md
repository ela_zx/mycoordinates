# README #

* This is proof of concept google action to demonstrate Location Permission request and response with Diagflow & Go-lang
* Version 1.0

### How do I get set up? ###

1. Create a Diagflow Project
2. Import the ZIP file to the Diagflow project
3. Create a Google action on Google Cloud Platform (GCP) 
4. Link the newly created GCP project in the Diagflow integrations (Google Assistant)