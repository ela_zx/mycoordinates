// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// Fullfilment for API.AI
type Fullfilment struct {
	Speech      string    `json:"speech"`
	DisplayText string    `json:"displayText,omitempty"`
	ContextOut  []Context `json:"contextOut,omitempty"` //Array of context objects like "contextOut": [{"name":"weather", "lifespan":2, "parameters":{"city":"Rome"}}]
	Source      string    `json:"source"`

	Data struct {
		Google struct {
			ExpectUserResponse bool `json:"expectUserResponse"` // DO NOT OMITEMPTY ON BOOLS!!
		} `json:"google"`
	} `json:"data"`
}
