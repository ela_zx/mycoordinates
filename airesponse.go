// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

// AIResponse represents the parsed user intent from the json request coming from api.ai
type AIResponse struct {
	OriginalRequest originalRequest `json:"originalRequest"`
	Result          struct {
		ResolvedQuery string            `json:"resolvedQuery"`
		Action        string            `json:"action"`
		Parameters    map[string]string `json:"parameters"`
		Contexts      []Context         `json:"contexts"`
		Metadata      struct {
			IntentName string `json:"intentName"`
		} `json:"metadata"`
	} `json:"result"`
	Status struct {
		Code int `json:"code"` // it should only be 200	success!!
	} `json:"status"`
}

// PermissionGranted returns true if permission to use device locaiton is granted and returns false otherwise
func (res *AIResponse) PermissionGranted() bool {
	for _, inp := range res.OriginalRequest.Data.Inputs {
		if inp.Intent == "actions.intent.PERMISSION" {
			for _, arg := range inp.Arguments {
				if arg.Name == "PERMISSION" {
					if arg.TextValue == "true" {
						return true
					}
					return false
				}
			}
			return false
		}
	}
	return false
}
