// Copyright 2017 Elias H. Tsigie. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package main

type RichResponseFulFillment struct {
	Speech      string `json:"speech"`
	DisplayText string `json:"displayText,omitempty"`
	Data        struct {
		Google struct {
			ExpectUserResponse bool `json:"expectUserResponse"` // DO NOT OMITEMPTY ON BOOLS!!
			//FinalResponse     FinalResponse `json:"finalResponse,omitempty"`
			RichResponse RichResponse `json:"richResponse,omitempty"`
		} `json:"google"`
	} `json:"data"`
}

// type FinalResponse struct {
// 	RichResponse RichResponse `json:"richResponse,omitempty"`
// }

type RichResponse struct {
	Items []interface{} `json:"items,omitempty"`
}
type ItemSR struct {
	SimpleResponse SimpleResponse `json:"simpleResponse,omitempty"`
}
type ItemBC struct {
	BasicCard BasicCard `json:"basicCard,omitempty"`
}

type SimpleResponse struct {
	TextToSpeech string `json:"textToSpeech"`
}
type BasicCard struct {
	Title         string   `json:"title"`
	SubTitle      string   `json:"subtitle"`
	FormattedText string   `json:"formattedText"`
	Buttons       []Button `json:"buttons"`
}
type Button struct {
	Title         string        `json:"title"`
	OpenURLAction OpenURLAction `json:"openUrlAction"`
}
type OpenURLAction struct {
	URL string `json:"url"`
}

func buildRichResponse(textToSpeech, title, subTitle, formattedText, url, urlTitle string) *RichResponseFulFillment {

	btn := Button{urlTitle, OpenURLAction{url}}
	basic := BasicCard{title, subTitle, formattedText, []Button{btn}}
	simple := SimpleResponse{textToSpeech}
	itemSR := ItemSR{simple}
	itemBC := ItemBC{basic}
	var items []interface{}
	items = append(items, itemSR)
	items = append(items, itemBC)
	rip := RichResponse{items}
	//final := FinalResponse{rip}

	rr := RichResponseFulFillment{}
	rr.Speech = textToSpeech
	rr.DisplayText = textToSpeech

	//rr.Data.Google.FinalResponse = final
	rr.Data.Google.RichResponse = rip

	return &rr

}
